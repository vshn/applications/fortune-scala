# Step 1: Builder image
FROM mozilla/sbt:8u292_1.5.7 AS builder
RUN apt-get update && apt-get install -y unzip
COPY src /fortune-build/src
COPY project /fortune-build/project
COPY build.sbt /fortune-build/build.sbt
WORKDIR /fortune-build
# Courtesy of https://github.com/sbt/sbt-native-packager
RUN sbt universal:packageBin
RUN unzip /fortune-build/target/universal/fortune-in-scala-1.0-scala.zip

# tag::production[]
# Step 2: Runtime image
FROM alpine:3.14
RUN apk add --no-cache fortune bash openjdk11-jre
WORKDIR /fortune
COPY --from=builder /fortune-build/fortune-in-scala-1.0-scala /fortune
EXPOSE 8080

# <1>
USER 1001:0

CMD ["/fortune/bin/fortune-in-scala"]
# end::production[]
