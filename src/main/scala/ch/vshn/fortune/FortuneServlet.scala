package ch.vshn.fortune

import org.scalatra._
import org.scalatra.json._
import sys.process._
import org.json4s.{DefaultFormats, Formats}

case class Fortune(message: String, version: String, number: Integer, hostname: String)

// tag::router[]
class FortuneServlet extends ScalatraServlet with JacksonJsonSupport {
  protected implicit lazy val jsonFormats: Formats = DefaultFormats
  protected val hostname = "hostname".!!
  protected val version = "1.2-scala"

  def fortune_message() : String = {
    return "fortune".!!
  }

  def random_number() : Integer = {
    return scala.util.Random.nextInt(1000)
  }

  get("/") {
    val number = random_number()
    val fortune = fortune_message()
    views.html.hello.render(fortune, hostname, number, version)
  }

  get("/", request.getHeader("Accept") == "application/json") {
    val number = random_number()
    val fortune = fortune_message()
    contentType = formats("json")
    Fortune(fortune, version, 123, hostname)
  }

  get("/", request.getHeader("Accept") == "text/plain") {
    val number = random_number()
    val fortune = fortune_message()
    "Fortune %s cookie of the day #%d:\n\n%s".format(version, number, fortune);
  }
}
// end::router[]
