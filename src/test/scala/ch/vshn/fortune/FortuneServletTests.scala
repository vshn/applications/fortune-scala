package ch.vshn.fortune

import org.scalatra.test.scalatest._

class FortuneServletTests extends ScalatraFunSuite {

  addServlet(classOf[FortuneServlet], "/*")

  test("GET / on FortuneServlet should return status 200") {
    get("/") {
      status should equal (200)
    }
  }

}
